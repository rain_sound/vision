# PROYECTO DE VISION COMPUTACIONAL - OPTICAL CHARACTER RECOGNITION
### Alumno: Arivilca Miranda Denis Ramiro
### Semestre: 9no
### Universidad Catolica de Santa Maria

Para probar la aplicación es necesario instalar [nodejs](https://nodejs.org/en/).
Instalar las dependencias e iniciar el servidor, con el último comando se abrirá una nueva pestaña en el navegador, si el puerto esta ocupado especificar otro.

```
$ npm install
$ npm i http-server
$ http-server -c-1 -o -p 8083
```
## Paper utilizado
[Optical Character Recognition - IJRTE](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.673.8061&rep=rep1&type=pdf)

## Funcionamiento
Una vez iniciado el servidor se procede a cargar una imagen desde el computador, posteriormente hacer click en cargar ideas, esperar a que la barra de progreso culmine de cargar y hacer click en crear mapa para visualizar el resultado.
